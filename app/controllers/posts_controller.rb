class PostsController < InheritedResources::Base
	private
  before_filter :authenticate, :except => [:index, :show]
  def authenticate
    authenticate_or_request_with_http_basic do |user_name, password|
      user_name == 'admin' && password == 'password'
    end
  end
end
