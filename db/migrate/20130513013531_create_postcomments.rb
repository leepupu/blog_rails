class CreatePostcomments < ActiveRecord::Migration
  def change
    create_table :postcomments do |t|
      t.string :commenter
      t.text :body
      t.references :post

      t.timestamps
    end
    add_index :postcomments, :post_id
  end
end
